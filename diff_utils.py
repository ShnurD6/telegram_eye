from enum import IntEnum

import diff_match_patch as dmp_module
from abc import ABC, abstractmethod


class DiffPurpose(IntEnum):
    TelegramMd = 0,
    TelegramHtml = 1,
    LinuxConsole = 2


class _NiceDiffHelper(ABC):

    @staticmethod
    @abstractmethod
    def highlight_old(text: str):
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def highlight_new(text: str):
        raise NotImplementedError()

    def get_nice_diff(self, first_text, second_text) -> str:
        dmp = dmp_module.diff_match_patch()
        diffs = dmp.diff_main(first_text, second_text)
        dmp.diff_cleanupSemantic(diffs)

        result = ""
        for (state_type, string) in diffs:
            if state_type == dmp.DIFF_DELETE:
                result += self.highlight_old(string)
            elif state_type == dmp.DIFF_INSERT:
                result += self.highlight_new(string)
            elif state_type == dmp.DIFF_EQUAL:
                result += string
            else:
                raise ValueError(f"Unknown diff state_type: {state_type}")

        return result


_LINUX_RED = '\033[9;31m'
_LINUX_GREEN = '\033[0;32m'
_LINUX_NOCOLOR = '\033[0m'


class _LinuxConsoleNiceDiff(_NiceDiffHelper):
    @staticmethod
    def highlight_old(text: str):
        return f"{_LINUX_RED}{text}{_LINUX_NOCOLOR}"

    @staticmethod
    def highlight_new(text: str):
        return f"{_LINUX_GREEN}{text}{_LINUX_NOCOLOR}"


# I want to underline messages in New, but telethon doesn't do that in Markdown yet. 
# Therefore, I do not use this class yet.
class _TelegramMdNiceDiff(_NiceDiffHelper):
    @staticmethod
    def highlight_old(text: str):
        return f"~~{text}~~"

    @staticmethod
    def highlight_new(text: str):
        # 
        return f"**__{text}__**"


class _TelegramHtmlNiceDiff(_NiceDiffHelper):
    @staticmethod
    def highlight_old(text: str):
        return f"<del>{text}</del>"

    @staticmethod
    def highlight_new(text: str):
        return f"<u><b>{text}</b></u>"


TelegramMdNiceDiff = _TelegramMdNiceDiff()
TelegramHtmlNiceDiff = _TelegramHtmlNiceDiff()
LinuxConsoleNiceDiff = _LinuxConsoleNiceDiff()


def GetHighlightedMessageDiff(first_message: str, second_message: str, purpose: DiffPurpose):
    if purpose == DiffPurpose.TelegramMd:
        return TelegramMdNiceDiff.get_nice_diff(first_message, second_message)
    elif purpose == DiffPurpose.TelegramHtml:
        return TelegramHtmlNiceDiff.get_nice_diff(first_message, second_message)
    elif purpose == DiffPurpose.LinuxConsole:
        return LinuxConsoleNiceDiff.get_nice_diff(first_message, second_message)
    else:
        raise ValueError("GetHighlightedMessageDiff: Unknown DiffPurpose")
