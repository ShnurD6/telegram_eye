import logging
from datetime import timedelta
from enum import IntEnum

from db import _GetSizeMBOfMessagesMediaTill, _GetCountOfMessagesTill

logger = logging.getLogger("TelegramEye")


def IsTextSafeModeEnabled(source_id):
    maxMessagesPerInterval = {
        timedelta(minutes=5): 50,
    }

    for key in maxMessagesPerInterval.keys():
        if _GetCountOfMessagesTill(source_id, key) > maxMessagesPerInterval[key]:
            logger.warning(f"Enable text safemode for source {source_id} by {key} limit")
            return True
    return False


def IsMediaSafeModeEnabled(source_id):
    maxMBsPerInterval = {
        timedelta(minutes=1): 50,
        timedelta(hours=1): 200,
        timedelta(hours=4): 400,
    }

    for key in maxMBsPerInterval.keys():
        sizeMB = _GetSizeMBOfMessagesMediaTill(source_id, key)
        if sizeMB > maxMBsPerInterval[key]:
            logger.warning(
                f"Enable media safemode for source {source_id} by {key} limit. " f"Current value = {sizeMB}MB")
            return True
    return False


class MsgTypeForCut(IntEnum):
    TextMessage = 1
    MediaCaption = 2


def CutMessageIfNeeded(mssg_txt: str, mssg_type: MsgTypeForCut):
    if mssg_type == MsgTypeForCut.TextMessage:
        max_message_len = 4096
    elif mssg_type == MsgTypeForCut.MediaCaption:
        max_message_len = 1024
    else:
        raise ValueError("Unknown Message Type")

    if len(mssg_txt) <= max_message_len:
        return mssg_txt
    return mssg_txt[:max_message_len - 3] + '...'
