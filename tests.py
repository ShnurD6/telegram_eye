import unittest

from utils import CutMessageIfNeeded, MsgTypeForCut
from diff_utils import *


class TelegramEyeUtilsTest(unittest.TestCase):

    def test_cut_message(self):
        long_message = 'a' * 10000

        cutted_message = CutMessageIfNeeded(long_message, MsgTypeForCut.TextMessage)
        expected_cutted_message = 'a' * 4093 + '...'
        self.assertEqual(4096, len(cutted_message))
        self.assertEqual(expected_cutted_message, cutted_message)

        cutted_message = CutMessageIfNeeded(long_message, MsgTypeForCut.MediaCaption)
        expected_cutted_message = 'a' * 1021 + '...'
        self.assertEqual(1024, len(cutted_message))
        self.assertEqual(expected_cutted_message, cutted_message)

        actualy_not_long_message = 'not_long_message'
        cutted_message = CutMessageIfNeeded(actualy_not_long_message, MsgTypeForCut.TextMessage)
        self.assertEqual(actualy_not_long_message, cutted_message)
        cutted_message = CutMessageIfNeeded(actualy_not_long_message, MsgTypeForCut.MediaCaption)
        self.assertEqual(actualy_not_long_message, cutted_message)

        pretty_long_text_message = 'a' * 4096
        cutted_message = CutMessageIfNeeded(pretty_long_text_message, MsgTypeForCut.TextMessage)
        self.assertEqual(pretty_long_text_message, cutted_message)

        pretty_long_media_caption = 'a' * 1024
        cutted_message = CutMessageIfNeeded(pretty_long_media_caption, MsgTypeForCut.MediaCaption)
        self.assertEqual(pretty_long_media_caption, cutted_message)

    def test_higlighted_diff(self):
        first_message = "Old message"
        second_message = "New message"

        self.assertEqual(
            GetHighlightedMessageDiff(first_message, second_message, DiffPurpose.LinuxConsole),
            f"{LinuxConsoleNiceDiff.highlight_old('Old')}{LinuxConsoleNiceDiff.highlight_new('New')} message")

        self.assertEqual(
            GetHighlightedMessageDiff(first_message, second_message, DiffPurpose.TelegramMd),
            f"~~Old~~**__New__** message")

        self.assertEqual(
            GetHighlightedMessageDiff(first_message, second_message, DiffPurpose.TelegramHtml),
            f"<del>Old</del><u><b>New</b></u> message")


if __name__ == '__main__':
    unittest.main()
