from enum import IntEnum
from typing import Optional

from telethon.hints import Entity
from telethon.tl import types


class SourceType(IntEnum):
    DirectMessage = 0
    Chat = 1
    Bot = 2
    Channel = 3

    Unknown = 99

    def str(self) -> str:
        _StringRepresentNames = {
            SourceType.DirectMessage: "DirectMessage",
            SourceType.Chat: "Chat",
            SourceType.Bot: "Bot",
            SourceType.Channel: "Channel"
        }

        return _StringRepresentNames.get(self, "")


def get_source_type_by_entity(entity: Entity) -> SourceType:
    if type(entity) == types.User:
        if entity.bot:
            return SourceType.Bot
        else:
            return SourceType.DirectMessage
    if type(entity) == types.Chat or (type(entity) == types.Channel and entity.megagroup):
        return SourceType.Chat
    if type(entity) == types.Channel:
        return SourceType.Channel

    return SourceType.Unknown


def get_name_by_entity(entity: Entity, source_type: SourceType) -> Optional[str]:
    if source_type == SourceType.Bot:
        return entity.first_name
    if source_type == SourceType.DirectMessage:
        return f"{entity.first_name} {entity.last_name}"
    if source_type in (SourceType.Chat, SourceType.Channel):
        return entity.title
    return None
