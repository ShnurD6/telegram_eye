import logging
from enum import IntEnum
import telethon.tl.types as types


class MediaType(IntEnum):
    NoneMedia = 0
    Photo = 1
    ExpiredPhoto = 2
    Document = 3
    VoiceNote = 4
    VideoNote = 5  # Кружочек))
    GifFile = 6
    Contact = 7
    Web = 8
    Video = 9
    ExpiredVoiceNote = 10
    ExpiredVideoNote = 11
    Sticker = 12

    Unknown = 99

    def __str__(self) -> str:
        _StringRepresentNames = {
            MediaType.Photo: "Photo",
            MediaType.ExpiredPhoto: "ExpiredPhoto",
            MediaType.Document: "Document",
            MediaType.VoiceNote: "VoiceNote",
            MediaType.VideoNote: "Кружочек",
            MediaType.Video: "Video",
            MediaType.GifFile: "Gif",
            MediaType.Contact: "Contact",
            MediaType.Web: "Web",
            MediaType.ExpiredVoiceNote: "ExpiredVoiceNote",
            MediaType.ExpiredVideoNote: "ExpiredVideoNote",
            MediaType.Sticker: "Sticker",
        }

        return _StringRepresentNames.get(self, "")

    def is_expiring(self):
        _expiring_messages = {
            MediaType.ExpiredPhoto,
            MediaType.ExpiredVoiceNote,
            MediaType.ExpiredVideoNote
        }

        return self in _expiring_messages

    def GetMediaTypeByMessage(message):
        message_media = message.media

        if message_media is None:
            return MediaType.NoneMedia

        _MediaTypesMap = {
            types.MessageMediaPhoto: MediaType.Photo,
            types.MessageMediaDocument: MediaType.Document,
            types.MessageMediaContact: MediaType.Contact,
            types.MessageMediaWebPage: MediaType.Web,
        }

        result = _MediaTypesMap.get(type(message_media), MediaType.NoneMedia)

        if message.voice:
            result = MediaType.VoiceNote
        if message.video:
            result = MediaType.Video
        if message.video_note:
            result = MediaType.VideoNote
        if message.gif:
            result = MediaType.GifFile
        if message.file is not None and message.file.sticker_set is not None:
            result = MediaType.Sticker

        _UpgradeTtlMap = {
            MediaType.Photo: MediaType.ExpiredPhoto,
            MediaType.VoiceNote: MediaType.ExpiredVoiceNote,
            MediaType.VideoNote: MediaType.ExpiredVideoNote
        }

        if type(message_media) is types.MessageMediaUnsupported:
            logging.error(f"Unimplemented media type in message: {message}")
            return MediaType.Unknown

        if result in _UpgradeTtlMap.keys() and message_media.ttl_seconds is not None:
            result = _UpgradeTtlMap.get(result, result)

        if message_media is not None and result is None:
            raise Exception(f"Handled unknown type of message.media: {type(message_media)}")

        return result
