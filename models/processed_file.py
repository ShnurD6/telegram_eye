from models.media_type import MediaType


class ProcessedFile:
    def __init__(self, media=None, media_size=0, media_type=MediaType.NoneMedia, file_name=None):
        self.media = media
        self.media_size = media_size
        self.media_type = media_type
        self.file_name = file_name

    def __repr__(self):
        return f"ProcessedFile[" \
               f"Media:{self.media}, " \
               f"MediaSize:{self.media_size}, " \
               f"MediaType:{self.media_type}" \
               f"Name:{self.file_name}" \
               f"]"

    def size_in_mb(self) -> str:
        return f"{str(self.media_size / 1024 / 1024)[:5]}MB"

    def get_short_repr(self) -> str:
        default_file_names = [
            "UnknownFileName.jpg",
            "UnknownFileName.mp4",
        ]
        types_where_name_is_not_needed = [
            MediaType.ExpiredPhoto,
            MediaType.VoiceNote,
            MediaType.ExpiredVoiceNote,
            MediaType.VideoNote,
            MediaType.ExpiredVideoNote,
            MediaType.Contact,
            MediaType.Web,
            MediaType.Sticker
        ]

        if self.file_name is None:
            return "None"

        type_str = MediaType(self.media_type)
        size_str = self.size_in_mb()

        if self.file_name in default_file_names or self.media_type in types_where_name_is_not_needed:
            return f"{type_str} ({size_str})"
        return f"{self.file_name} ({type_str}, {size_str})"
