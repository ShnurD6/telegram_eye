import logging
import sys
import threading
import traceback
from datetime import timedelta

from zstd import ZSTD_compress, ZSTD_uncompress

import db

# The original idea behind the compression feature was as follows:
#   All files stored in the database older than 7 days are compressed using ZSTD to save space.
#   When sent to private messages, these files will be unpacked.
#   Compression is logged to a separate file: `compressor.log`
#
# The compression feature was released in September 2022 and has since been rolled back. Sorry!
# The fact is that Telegram Server itself compresses media quite well.
# As a result of the launch of the compression feature, it turned out that the benefit
# of compressing 6GB of media was about ~ 75mb, so the feature was recognized as unprofitable,
# given that compression consumes a lot of machine resources.
#
# It is possible to run the feature back by reproducing the steps from commit "1f5efd12d06c8e7482b950bf12eb3ef97e11233e"


compressor_handler = logging.FileHandler("compressor.log")
compressor_handler.setFormatter(logging.Formatter('[%(levelname) 5s/%(asctime)s] %(message)s'))

logger = logging.getLogger("Compressor")
logger.setLevel(logging.DEBUG)
logger.propagate = False
logger.addHandler(compressor_handler)


def try_to_compress_old_media_loop():
    try:
        uncompressed = db.GetUncompressedMessages(timedelta(seconds=1))

        if len(uncompressed) == 0:
            threading.Timer(60, try_to_compress_old_media_loop).start()
            return

        for messageRow in uncompressed:
            message = messageRow.Message

            assert message.media is not None

            compressed_media = ZSTD_compress(message.media, 22)
            logger.debug(
                f"Compress message #{message.id} media | {sys.getsizeof(message.media)} -> {sys.getsizeof(compressed_media)} "
                f"({sys.getsizeof(compressed_media) / (sys.getsizeof(message.media) / 100)}%)")

            db.UploadCompressedMedia(message.id, compressed_media, sys.getsizeof(compressed_media))

        try_to_compress_old_media_loop()
    except:
        logger.error(traceback.format_exc())

def try_to_decompress_old_media_loop():
    try:
        compressed = db.GetCompressedMessages()

        if len(compressed) == 0:
            return

        for messageRow in compressed:
            message = messageRow.Message

            assert message.media is not None

            decompressed_media = decompress_media(message.media)
            logger.debug(
                f"Decompress message #{message.id} media | {sys.getsizeof(message.media)} -> {sys.getsizeof(decompressed_media)} "
                f"({sys.getsizeof(decompressed_media) / (sys.getsizeof(message.media) / 100)}%)")

            db.UploadDecompressedMedia(message.id, decompressed_media)

        try_to_decompress_old_media_loop()
    except:
        logger.error(traceback.format_exc())


def decompress_media(media):
    return ZSTD_uncompress(media)


def compress_enable():
    threading.Timer(1, try_to_compress_old_media_loop).start()
