import logging
from typing import Optional

import db_connection_string
import config
from datetime import datetime, timedelta

from sqlalchemy import VARCHAR, Column, DateTime, Integer, SmallInteger, create_engine, ForeignKey
from sqlalchemy import func
from sqlalchemy import select, update
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import desc
from sqlalchemy.sql.sqltypes import Boolean, LargeBinary, BigInteger

from models.processed_file import ProcessedFile
from models.source_type import SourceType

engine = create_engine(db_connection_string.DB_CONNECTION_STR, echo=False)

Session = sessionmaker(bind=engine)
Base = declarative_base()

logger = logging.getLogger("TelegramEye")
logging.getLogger('sqlalchemy.engine').setLevel(logging.WARNING)


class Message(Base):
    __tablename__ = "messages"
    id = Column(BigInteger, primary_key=True)
    creation_timestamp = Column(DateTime, nullable=False)
    source_id = Column(BigInteger)
    message = Column(VARCHAR)
    reply_to = Column(Integer, default=None)
    media = Column(LargeBinary)
    media_type = Column(SmallInteger)
    media_size = Column(Integer, default=0, nullable=False)
    is_deleted = Column(Boolean, default=False, nullable=False)
    is_reported = Column(Boolean, default=False, nullable=False)
    file_name = Column(VARCHAR)

    def __init__(self, message_id, creation_timestamp, source_id, message, processed_file, reply_to):
        self.id = message_id
        self.creation_timestamp = creation_timestamp
        self.source_id = source_id
        self.message = message
        self.reply_to = reply_to
        self.media = processed_file.media
        self.media_type = processed_file.media_type
        self.media_size = processed_file.media_size
        self.file_name = processed_file.file_name


    def __repr__(self) -> str:
        return "Message{" \
               f"Id:{self.id} | " \
               f"CreationTimestamp:{self.creation_timestamp} |" \
               f"SourceId:{self.source_id} |" \
               f"Text:{self.message} | " \
               f"ReplyTo:{self.reply_to} | " \
               f"Media:{type(self.media)} |" \
               f"MediaType:{self.media_type} |" \
               f"MediaSize:{self.media_size} | " \
               f"FileName:{self.file_name}" \
               "}"

    def GetShortMessageLog(self, source_name=None) -> str:
        media = ProcessedFile(
            media=self.media,
            media_size=self.media_size,
            media_type=self.media_type,
            file_name=self.file_name)

        return f" Source: {source_name if source_name is not None else GetSourceById(self.source_id).Source.name} |" \
               f" Id: {self.id} |" \
               f" {self.message} |" \
               f" Media: {media.get_short_repr()}"


class Source(Base):
    __tablename__ = "sources"
    id = Column(BigInteger, primary_key=True)
    name = Column(VARCHAR)
    type = Column(SmallInteger, default=0, nullable=False)
    need_store = Column(Boolean, default=True, nullable=False)

    def __init__(self, id, name, type, need_store):
        self.id = id
        self.name = name
        self.type = type
        self.need_store = need_store

    def __repr__(self) -> str:
        return "Source{" \
               f"Id:{self.id} | " \
               f"Name:{self.name} | " \
               f"Type:{str(SourceType(self.type))} | " \
               f"Need Store:{self.need_store}" \
               "}"

    def get_descr(self) -> str:
        _StringRepresentNames = {
            SourceType.DirectMessage: f"DM with {self.name}",
            SourceType.Chat: f"{self.name} chat",
            SourceType.Bot: f"{self.name} bot",
            SourceType.Channel: f"{self.name} channel"
        }

        return _StringRepresentNames.get(self.type, "")


class EditedMessage(Base):
    __tablename__ = "edited_messages"
    ref_message_id = Column('ref_message_id', Integer, ForeignKey('messages.id'), nullable=False, primary_key=True)
    source_id = Column(BigInteger)
    version = Column('version', Integer(), nullable=True, primary_key=True)
    editing_timestamp = Column('editing_timestamp', DateTime(), nullable=False)
    message = Column('message', VARCHAR(), nullable=True)
    media = Column('media', LargeBinary(), nullable=True)
    media_size = Column('media_size', Integer(), nullable=False)
    media_type = Column('media_type', SmallInteger(), nullable=True)
    is_reported = Column('is_reported', Boolean(), default=False, nullable=False)
    file_name = Column(VARCHAR)

    def __init__(self, ref_message_id, source_id, message, processed_file):
        self.ref_message_id = ref_message_id
        self.source_id = source_id
        self.message = message
        self.media = processed_file.media
        self.media_size = processed_file.media_size
        self.media_type = processed_file.media_type
        self.file_name = processed_file.file_name

    def __repr__(self) -> str:
        return "EditedMessage{" \
               f"ref_message_id:{self.ref_message_id} | " \
               f"source_id:{self.source_id} | " \
               f"version:{self.version} | " \
               f"message:{self.message} | " \
               f"media:{self.media} | " \
               f"media_size:{self.media_size} | " \
               f"media_type:{self.media_type} | " \
               f"file_name:{self.file_name}" \
               "}"


def GetAddDeletedUnsendedMessages():
    with Session() as session:
        deleted_messages = session.execute(
            select(Message) \
                .where(Message.is_deleted == True, Message.is_reported == False) \
                .order_by(Message.creation_timestamp.desc())
                .limit(config.deleted_messages_per_request)) \
            .fetchall()

    return deleted_messages


def GetAddEditedUnsendedMessagesByRef(ref_message_id):
    with Session() as session:
        edited_messages = session.execute(
            select(EditedMessage) \
                .where(EditedMessage.is_reported == False, EditedMessage.ref_message_id == ref_message_id) \
                .order_by(EditedMessage.editing_timestamp)) \
            .fetchall()

    return edited_messages


def GetAddEditedUnsendedMessagesIds():
    with Session() as session:
        edited_messages = session.execute(
            select(func.distinct(EditedMessage.ref_message_id, EditedMessage.source_id)) \
                .where(EditedMessage.is_reported == False) \
                .limit(config.edited_messages_per_request)) \
            .fetchall()

    return edited_messages


def GetMessageWithUniqueId(message_id) -> Optional[Message]:
    with Session() as session:
        res = session.execute(
            select(Message) \
                .where(Message.id == message_id)) \
            .fetchall()
        return res[0] if res is not None and len(res) == 1 else None


def GetMessageByIdAndSource(message_id, source_id) -> Optional[Message]:
    assert message_id is not None
    assert source_id is not None
    with Session() as session:
        res = session.execute(
            select(Message) \
                .where(
                    (Message.id == message_id) &
                    (Message.source_id == source_id))) \
            .fetchall()
        return res[0] if res is not None and len(res) == 1 else None


def GetEditedMessageByIdAndVersion(ref_message_id, source_id, version) -> Message:
    with Session() as session:
        res = session.execute(
            select(EditedMessage.message) \
                .where(
                    (EditedMessage.ref_message_id == ref_message_id) &
                    (EditedMessage.source_id == source_id) &
                    (EditedMessage.version == version))) \
            .fetchone()
        return res[0] if res is not None and len(res) == 1 else None


def GetSourceById(source_id) -> Optional[Source]:
    with Session() as session:
        res = session.execute(
            select(Source) \
                .where(Source.id == source_id)) \
            .fetchall()
        return res[0] if res is not None and len(res) == 1 else None


def AddSource(source):
    dublicate_source_raw = GetSourceById(source.id)
    if dublicate_source_raw:
        dublicate_source = dublicate_source_raw.Source
        if source.name == dublicate_source.name:
            # Most likely, several messages in the new source arrived at the same time and asynchronously
            logger.warning(f"Retrying to add a source {source}.")
        else:
            raise Exception(f"Duplicate source_id: {dublicate_source} vs {source}")
        return

    with Session() as session:
        session.add(source)
        session.commit()


def SetMessageReported(message_id, source_id):
    assert message_id is not None

    with Session() as session:
        session.execute(
            update(Message) \
                .where(
                    (Message.id == message_id) &
                    (Message.source_id == source_id))
                .values(is_reported=True))
        session.commit()


def SetEditMessageReported(ref_message_id, source_id):
    assert ref_message_id is not None

    with Session() as session:
        session.execute(
            update(EditedMessage) \
                .where(
                    (EditedMessage.ref_message_id == ref_message_id) &
                    (EditedMessage.source_id == source_id))
                .values(is_reported=True))
        session.commit()


def GetDbSize() -> str:
    with engine.connect() as con:
        query = "SELECT pg_size_pretty(pg_database_size('telegram_eye'));"
        res = con.execute(query)

    return res.fetchone()[0]


def SetMessageDeleted(message_id, source: Optional[Source]):
    assert message_id is not None

    deletedMesssage = GetMessageWithUniqueId(message_id) if source is None \
        else GetMessageByIdAndSource(message_id, source.id)

    if not deletedMesssage:
        if not source or source.need_store:
            logger.warning(f"Message {message_id} deleted, but not exist in DB :( "
                           f"| Source: {source.name if source else 'Unknown'}")
        return

    logger.warning(f"[DELETED] {deletedMesssage.Message.GetShortMessageLog()}")

    if source:
        with Session() as session:
            session.execute(
                update(Message) \
                    .where(
                        (Message.id == message_id) &
                        (Message.source_id == source.id))
                    .values(is_deleted=True))
            session.commit()
    else:
        with Session() as session:
            session.execute(
                update(Message) \
                    .where(Message.id == message_id)
                    .values(is_deleted=True))
            session.commit()


def _GetCountOfMessagesTill(source_id, duration: timedelta) -> int:
    begin_range_time = datetime.now() - duration

    with Session() as session:
        messages_count = session.execute(
            select(func.count('*')) \
                .where(
                Message.source_id == source_id,
                Message.creation_timestamp > begin_range_time)) \
            .fetchone()[0]

    return messages_count


def _GetNewEditVersion(ref_message_id, source_id) -> int:
    with Session() as session:
        last_version = session.execute(
            select(func.max(EditedMessage.version)) \
                .where(
                    (EditedMessage.ref_message_id == ref_message_id) &
                    (EditedMessage.source_id == source_id))) \
            .fetchone()[0]

    return int(last_version) + 1 if last_version is not None else 1


def _GetSizeMBOfMessagesMediaTill(source_id, duration: timedelta) -> int:
    begin_range_time = datetime.now() - duration

    with Session() as session:
        result_in_bytes = session.execute(
            select(func.sum(Message.media_size)) \
                .where(
                Message.source_id == source_id,
                Message.creation_timestamp > begin_range_time)) \
            .fetchone()[0]

    return int(result_in_bytes) / 1024 / 1024 if result_in_bytes else 0


def AppendTextToLastMessage(source_id, new_message):
    with Session() as session:
        old_row = session.query(Message) \
            .filter(Message.source_id == source_id) \
            .order_by(desc(Message.creation_timestamp)) \
            .first()
        logger.debug(f'[AppendTextToLastMessage] old_row = {old_row}')
        old_row.message += f" | {new_message}"
        session.commit()


def AddEditOfMessage(editedMessage: EditedMessage):
    assert editedMessage.ref_message_id is not None

    editedMessage.version = _GetNewEditVersion(editedMessage.ref_message_id, editedMessage.source_id)
    editedMessage.editing_timestamp = datetime.now()

    with Session() as session:
        session.add(editedMessage)
        session.commit()


def SaveMessage(message, message_id, source_id, processed_file, reply_to):
    assert message is not None
    assert message_id is not None

    with Session() as session:
        message = Message(
            message_id=message_id,
            creation_timestamp=datetime.now(),
            source_id=source_id,
            message=message,
            processed_file=processed_file,
            reply_to=reply_to)

        session.add(message)
        session.commit()
