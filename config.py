# This file contains configuration variables for customizing the behavior of Telegram Eye.

# Limits the number of deleted messages sent by the /open command
deleted_messages_per_request = 20

# Limits the number of deleted messages sent by the /open command
edited_messages_per_request = 20

# Should the running client handle /open, or should it just save everything to the database
reporting_enable = True

# Database name
db_name = 'telegram_eye'

# Name of database user
db_user = 'telegram_eye'

# Password of database user
db_password = 'telegram_eye'

# Host of database
db_host = 'localhost'

# Port of database
db_port = 1337
