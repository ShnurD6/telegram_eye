## Telegram Eye

###### Python daemon for saving / sending deleted messages

--------------------------------------------------------

##### Installation:

1) `pip3 install -r requirements.txt`

2) Setup Database `docker run -p 1337:5432 -e POSTGRES_USER=telegram_eye -e POSTGRES_PASSWORD=telegram_eye -e POSTGRES_DB=telegram_eye -v $(pwd)/volume:/var/lib/postgresql/data -d postgres:latest`

3) `alembic upgrade head`   

4) Set ENV variables `API_ID` and `API_HASH` from https://my.telegram.org

##### Run:
python3 message_handler.py

##### Use:
Send `/open` to Saved Messages in your Telegram account, 
deleted and edited messages will be sent to you, if any. 

By default, only private messages are saved. 
To save chat messages, channels or bots, put `need_store=true` 
in the database against the source you are interested 
in the "sources" table.

##### SafeMode:

SafeMode is a feature that allows you to save your
database or server space from spammers. 
Unfortunately, at the moment, all thresholds are hardcoded. 
In the future, the threshold values will be added to the config.
Currently not supported for editing messages.

There are two kinds of Safe Mode: Text and Media. 

###### Text Safe Mode
If the interlocutor sent more than 15 messages in a minute 
or more than 35 in 5 minutes, all subsequent messages 
from this interlocutor will be glued into one.

###### Media Safe Mode
If the interlocutor has sent more than 50 MB in the last minute
or more than 200 MB in the last hour or more than 400 MB in the
last 4 hours, media from this interlocutor will not be downloaded.

Also, messages with media attachments over 50 MB are not downloaded.

##### Compression of media
The compression feature was released in September 2022 and has since been rolled back. 
All details in the file media_compressor.py.

------------------------------------------------------------------------------

###### TODO (in priority order):

- Hints about sticker name representation and voice messages description in logs.
- Handle Geolocation/Contacts
- Upload progress bar for deleted messages/attachments
- Switching between all-chats-mode and only-private-chats-mode in the config
- Ability to display images (for example, with [Fbi](https://www.nongnu.org/fbi-improved/))
- The ability to store large files not in a database, but on Telegram servers. (For example, send to some chat and take from there when deleted)
