import asyncio
import logging
from os import getenv
from typing import Optional

from telethon import TelegramClient, events

import config
import db
from diff_utils import *
import utils
from models.processed_file import ProcessedFile
from models.media_type import MediaType
from models.source_type import SourceType, get_source_type_by_entity, get_name_by_entity


async def GetMe():
    global client, me
    me = await client.get_me()


if __name__ == "__main__":
    logger = logging.getLogger("TelegramEye")

    logging.getLogger("telethon").setLevel(logging.WARNING)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("asyncio").setLevel(logging.WARNING)

    logging.basicConfig(format="[%(levelname) 5s/%(asctime)s] %(message)s", level=logging.DEBUG)

    api_id = int(getenv("API_ID"))
    api_hash = getenv("API_HASH")

    client = TelegramClient("MessagesHandler", api_id, api_hash)
    client.start()

    max_media_size_bytes = 50 * 1024 * 1024

    me = None
    asyncio.get_event_loop()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(GetMe())


async def GetSource(source_id, message: Optional[str] = None) -> db.Source:
    result_source = db.GetSourceById(source_id)
    if result_source:
        return result_source

    tlEntity = None

    try:
        tlEntity = await client.get_entity(source_id)
    except ValueError:
        # Dirty hack: refresh Telethon hashes
        await client.get_dialogs()

    # Second chance after dirty hack
    if tlEntity is None:
        try:
            tlEntity = await client.get_entity(source_id)
        except ValueError:
            # Most likely need to use InputEntity.
            logger.error(f"Cannot find entity by id: {source_id}. Message: {message}")

    source_type = get_source_type_by_entity(tlEntity)
    source_name = get_name_by_entity(tlEntity, source_type)

    if source_name is None:
        logger.error(f"Source_name is None! "
                     f"tlEntity={tlEntity}, "
                     f"source_type={source_type}, "
                     f"source_id={source_id}, "
                     f"message={message}")

    if len(source_name) > 50:
        truncated_source_name = source_name[0:50]
        logger.warning(f"Truncate source_name {source_name} -> {truncated_source_name}")
        source_name = truncated_source_name

    source = db.Source(
        id=source_id,
        name=source_name,
        type=source_type,
        # TODO: Ability to select NeedStore depending on the source type in the config.
        need_store=source_type == SourceType.DirectMessage)

    logger.info(f"Add new source: {source}")

    db.AddSource(source)
    # Validate
    return db.GetSourceById(source_id)


async def SendAllDeletedMessages():
    deleted_messages_rows = db.GetAddDeletedUnsendedMessages()
    if len(deleted_messages_rows) == 0:
        return
    logger.info(f"Process SendAllDeletedMessages! Deleted messages count is {len(deleted_messages_rows)}")

    previous_source_id = None
    for messageRow in deleted_messages_rows:
        message = messageRow.Message
        message_text = message.message
        source_id = message.source_id

        if previous_source_id != source_id:
            previous_source_id = source_id
            source = (await GetSource(source_id)).Source
            await client.send_message("me", f"====== Deleted messages in {source.get_descr()}: ======")

        if MediaType(message.media_type).is_expiring():
            message_text += "| Expired"

        if message.media is not None:
            media = await client.upload_file(
                bytes(message.media),
                file_name=message.file_name)

            await client.send_file(
                "me",
                media,
                voice_note=(message.media_type == MediaType.VoiceNote),
                video_note=(message.media_type == MediaType.VideoNote),
                caption=utils.CutMessageIfNeeded(message_text, utils.MsgTypeForCut.MediaCaption))
        else:
            await client.send_message("me", utils.CutMessageIfNeeded(message_text, utils.MsgTypeForCut.TextMessage))

        db.SetMessageReported(message.id, source_id)


async def SendAllEditedMessages():
    edited_messages_ref_ids_rows = db.GetAddEditedUnsendedMessagesIds()
    if len(edited_messages_ref_ids_rows) == 0:
        return

    logger.info(f"Edited messages count is {len(edited_messages_ref_ids_rows)}")

    previous_source_id = None
    for editedMessageRefIdRow in edited_messages_ref_ids_rows:
        # Need parse string cause distinct() use in GetAddEditedUnsendedMessagesIds().
        ref_id, source_id = str(editedMessageRefIdRow[0])[1:-1].split(',')

        if previous_source_id != source_id:
            previous_source_id = source_id
            source = (await GetSource(source_id)).Source
            await client.send_message("me", f"====== Edited messages in {source.get_descr()}: ======    ")

        messages_to_client = []
        previous_message = None
        for edited_message_row in db.GetAddEditedUnsendedMessagesByRef(ref_id):
            edited_message = edited_message_row.EditedMessage

            if previous_message is None:
                if edited_message.version == 1:
                    previous_message = db.GetMessageByIdAndSource(ref_id, source_id).Message.message
                else:
                    previous_message = db.GetEditedMessageByIdAndVersion(ref_id, source_id, edited_message.version - 1)

            messages_to_client.append(
                f"[{edited_message.version}] "
                f"{GetHighlightedMessageDiff(previous_message, edited_message.message, DiffPurpose.TelegramHtml)}")
            previous_message = edited_message.message

        client_message = " -> ".join(messages_to_client)
        await client.send_message(
            "me",
            utils.CutMessageIfNeeded(
                client_message,
                utils.MsgTypeForCut.TextMessage),
            parse_mode='html')
        db.SetEditMessageReported(ref_id, source_id)


@client.on(events.MessageDeleted())
async def deleted_messages_handler(event):
    source_raw = await GetSource(event.chat_id) if event.chat_id is not None else None
    source = source_raw.Source if source_raw else None

    if source and not source.need_store:
        return

    for msg_id in event.deleted_ids:
        db.SetMessageDeleted(msg_id, source)


@client.on(events.MessageEdited())
async def edited_messages_handler(event):
    message = event.message
    message_text = message.message
    source = (await GetSource(event.chat_id)).Source

    if not source.need_store:
        return

    oldMessage = db.GetMessageByIdAndSource(message.id, event.chat_id)
    if not oldMessage:
        logger.info(f"[E] Unknown message -> {message_text} | Source: {source.name}")
        return

    log = f"[E] Source: {source.name} " \
          f"| {GetHighlightedMessageDiff(oldMessage.Message.message, message_text, DiffPurpose.LinuxConsole)} " \
          f"| Id:{message.id}"
    log = await append_reply_to_log(log, message)

    processed_file, log, message_text = await process_file(log, message, message_text)

    if processed_file.media and processed_file.media == oldMessage.Message.media:
        processed_file.media = None
        log += f" | Skip equal media"

    if message_text == oldMessage.Message.message and not processed_file.media:
        # Possible reactions?
        return

    logger.debug(log)

    # Text safe mod needs to be improved to work for editing messages
    # if utils.IsTextSafeModeEnabled(event.chat_id):
    #     return db.AppendTextToLastMessage(event.chat_id, message_text)

    # Also needs improvement
    # if utils.IsMediaSafeModeEnabled(event.chat_id):
    #     media = None
    #     message = f"{message}| Media removed by SafeMode"

    if (not message_text or len(message_text) == 0) and processed_file.media is None:
        # Maybe contact, or geolocation
        return

    editedMessage = db.EditedMessage(
        ref_message_id=message.id,
        source_id=event.chat_id,
        message=message_text,
        processed_file=processed_file)
    db.AddEditOfMessage(editedMessage)

    if processed_file.media_type.is_expiring():
        db.SetMessageDeleted(message.id, source)


async def append_reply_to_log(log, message):
    if message.reply_to_msg_id:
        log += f" | ReplyTo:{message.reply_to_msg_id}"
    return log


@client.on(events.NewMessage(outgoing=True, pattern="/open", from_users=me.id))
async def process_open_command(event):
    if not config.reporting_enable:
        logger.info("Ignore /open command due to config.")
        return

    await client.edit_message(event.message, "Processing...")
    await SendAllEditedMessages()
    await SendAllDeletedMessages()
    await client.edit_message(event.message, "Processing completed!")
    await asyncio.sleep(1)
    await client.delete_messages(event.chat_id, [event.id])
    await client.send_message("me", f"All sended! Actual DB size is {db.GetDbSize()}")


@client.on(events.NewMessage())
async def new_message_handler(event):
    source = (await GetSource(event.chat_id, event.raw_text)).Source
    if not source.need_store:
        return

    message = event.message
    message_text = message.message

    log = f"[N] Source: {source.name} | {message_text} | Id:{message.id}"
    log = await append_reply_to_log(log, message)

    processed_file, log, message_text = await process_file(log, message, message_text)

    logger.debug(log)

    if utils.IsTextSafeModeEnabled(event.chat_id):
        return db.AppendTextToLastMessage(event.chat_id, message_text)

    if utils.IsMediaSafeModeEnabled(event.chat_id):
        processed_file.media = None
        message_text = f"{message_text}| Media removed by SafeMode"

    if (not message_text or len(message_text) == 0) and processed_file.media is None:
        # Maybe contact, or geolocation
        return

    db.SaveMessage(
        message=message_text,
        message_id=message.id,
        source_id=event.chat_id,
        processed_file=processed_file,
        reply_to=message.reply_to_msg_id)

    if processed_file.media_type.is_expiring():
        db.SetMessageDeleted(message.id, source)


async def process_file(log, message, message_text):
    result = ProcessedFile()

    if not message.file:
        return result, log, message_text

    result.media_size = message.file.size
    result.media_type = MediaType.GetMediaTypeByMessage(message)

    result.file_name = message.file.name
    if result.file_name is None:
        result.file_name = f"UnknownFileName{message.file.ext}"

    if result.media_size > max_media_size_bytes:
        logger.warning(f"Media skipped due to large file size")
        message_text += f"| Skipped attached media: {result.get_short_repr()}"
        return result, log, message_text

    result.media = await client.download_media(message, file=bytes)
    if result.media_type == MediaType.NoneMedia and result.media is not None:
        result.media_type = MediaType.Unknown
    log += f" | Media: {result.get_short_repr()}"

    return result, log, message_text


client.run_until_disconnected()
