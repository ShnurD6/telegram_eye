# Autogenerated file by alembic
# (See https://alembic.sqlalchemy.org/en/latest/tutorial.html#the-migration-environment for more info)

"""add type and ref to messages

Revision ID: f6d1e2f824c2
Revises: 85a47295c2e6
Create Date: 2022-04-17 06:04:28.307129

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f6d1e2f824c2'
down_revision = '85a47295c2e6'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('edited_messages',
        sa.Column('ref_message_id', sa.BigInteger(), sa.ForeignKey('messages.id'), nullable=False),
        sa.Column('version', sa.Integer(), nullable=True),
        sa.Column('editing_timestamp', sa.DateTime(), nullable=False),
        sa.Column('message', sa.VARCHAR(), nullable=True),
        sa.Column('media', sa.LargeBinary(), nullable=True),
        sa.Column('media_size', sa.Integer(), nullable=False),
        sa.Column('media_type', sa.Integer(), nullable=True),
        sa.Column('is_reported', sa.Boolean(), nullable=False))
    op.create_primary_key(
        "pk_edited_messages", "edited_messages",
        ["ref_message_id", "version"])


def downgrade():
    op.drop_table('edited_messages')
    pass
