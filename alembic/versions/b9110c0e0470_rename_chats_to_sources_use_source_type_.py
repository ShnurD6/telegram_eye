# Autogenerated file by alembic
# (See https://alembic.sqlalchemy.org/en/latest/tutorial.html#the-migration-environment for more info)

"""rename chats to sources, use source_type instead of is_bot

Revision ID: b9110c0e0470
Revises: 233b3818ff87
Create Date: 2022-09-17 18:39:25.826029

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b9110c0e0470'
down_revision = '233b3818ff87'
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table("chats", "sources")
    op.execute("""
           ALTER TABLE sources ADD COLUMN type SMALLINT DEFAULT 0 NOT NULL;
           UPDATE sources SET type = 2 WHERE is_bot = TRUE;
           ALTER TABLE sources DROP COLUMN is_bot;
       """)
    op.alter_column('messages', 'chat_id', new_column_name='source_id')
    pass


def downgrade():
    op.rename_table("sources", "chats")
    op.execute("""
               ALTER TABLE chats ADD COLUMN is_bot BOOL DEFAULT FALSE NOT NULL;
               UPDATE chats SET is_bot = TRUE WHERE type = 2;
               ALTER TABLE chats DROP COLUMN type;
           """)
    op.alter_column('messages', 'source_id', new_column_name='chat_id')
    pass
